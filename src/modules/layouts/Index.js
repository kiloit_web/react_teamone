import React, { useState } from "react";
import { Container, Nav, Navbar, NavDropdown } from "react-bootstrap";
import "../../assets/css/main.css";
import { Link } from "react-router-dom";
import SearchCom from "../../components/SearchCom";

function Index() {
  const [, setShowSearch] = useState(false);
  return (
    <>
      <Navbar
        expand="xl"
        className=" px-lg-5 px-sm-5 px-2 position-sticky top-0 bg-white z-2 menu"
      >
        <Container fluid>
          <Navbar.Brand
            as={Link}
            to="/"
            className="fw-bold fs-1 text-color1"
            style={{ letterSpacing: "-1px", color: "var(--color-primary)" }}
          >
            <i className="fa-solid fa-user-secret"></i> Startup
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="ms-auto fs-6 fw-bold">
              <Nav.Link as={Link} to="/" className="ms-lg-2 ">
                Home
              </Nav.Link>
              <Nav.Link as={Link} to="/about" className="ms-lg-2">
                About
              </Nav.Link>
              <Nav.Link as={Link} to="/services" className="ms-lg-2">
                Services
              </Nav.Link>
              <NavDropdown title="Blog" id="blog-dropdown" className="ms-lg-2">
                <NavDropdown.Item as={Link} to="/blogGrid">
                  Blog Grid
                </NavDropdown.Item>
                <NavDropdown.Item as={Link} to="/BlogDetail">
                  Blog Detail
                </NavDropdown.Item>
              </NavDropdown>
              <NavDropdown title="Page" id="page-dropdown" className="ms-lg-2">
                <NavDropdown.Item as={Link} to="/pricingPlan">
                  Pricing Plan
                </NavDropdown.Item>
                <NavDropdown.Item as={Link} to="/ourFeatures">
                  Our features
                </NavDropdown.Item>
                <NavDropdown.Item as={Link} to="/teamMembers">
                  Team Members
                </NavDropdown.Item>
                <NavDropdown.Item as={Link} to="/tesimonial">
                  Tesimonial
                </NavDropdown.Item>
                <NavDropdown.Item as={Link} to="/freeQuote">
                  Free Quote
                </NavDropdown.Item>
              </NavDropdown>

              <Nav.Link as={Link} to="/contact" className="ms-lg-2">
                Contact
              </Nav.Link>
              <Nav.Link className="ms-lg-2">
                <div
                  className="border-0 bg-white"
                  onClick={() => setShowSearch(true)}
                >
                  {<SearchCom handleClose={() => setShowSearch(false)} />}
                </div>
              </Nav.Link>

              <button
                className="border-0 bg-white"
                style={{ width: "max-content" }}
              >
                <Nav.Link
                  href="https://htmlcodex.com/startup-company-website-template/"
                  className="text-white  ms-lg-2 p-2"
                  style={{ backgroundColor: "var(--color-primary)" }}
                >
                  Download Pro Version
                </Nav.Link>
              </button>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </>
  );
}

export default Index;
