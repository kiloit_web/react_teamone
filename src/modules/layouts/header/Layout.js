import React from 'react'
import Index from '../Index'
import { Outlet } from 'react-router-dom'

const Layout = () => {
  return (
    <div>
      <Index/>
      <Outlet/>
    </div>
  )
}

export default Layout