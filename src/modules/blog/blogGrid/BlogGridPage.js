import React from "react";
import VendorCarousel from "../../../components/VendorCarousel";
import Footer from "../../layouts/footer/Footer";
import BackToTopCom from "../../../components/BackToTopCom";
import Image1 from "../../../assets/image/blog-1.jpg";
import Image2 from "../../../assets/image/blog-2.jpg";
import Image3 from "../../../assets/image/blog-3.jpg";

const BlogGridPage = () => {
  const handleClick = () => {
    window.scrollTo(0, 0);
  };
  return (
    <div>
      <div className="container margin-3rem">
        <div className="row">
          <div className="col-lg-8 col-md-12">
            <div className="row">
              <div className="col-md-6 col-sm-12 blog-grid-item mb-5">
                <div className="image position-relative">
                  <img src={Image1} alt="Image_Blog" className=" img-fluid" />
                  <div className="web-design text-white">
                    <a
                      onClick={handleClick}
                      href="#noPagenoPage"
                      className=" text-decoration-none text-white px-4 py-3"
                      style={{ backgroundColor: "var(--color-primary)" }}
                    >
                      Web Design
                    </a>
                  </div>
                </div>
                <div
                  style={{ backgroundColor: "var(--color-light-primary)" }}
                  className="p-3"
                >
                  <div className=" d-flex justify-content-between pt-2 mb-2">
                    <div>
                      <i
                        className="fa-solid fa-user me-2"
                        style={{ color: "var(--color-primary)" }}
                      ></i>{" "}
                      <span>Kim Hak</span>
                    </div>
                    <div>
                      <i
                        className="fa-solid fa-calendar-days me-2"
                        style={{ color: "var(--color-primary)" }}
                      ></i>{" "}
                      <span>01 Jan, 2024</span>
                    </div>
                  </div>
                  <h4 className="fw-bold ">How to build a website</h4>
                  <p>
                    Dolor et eos labore stet justo sed est sed sed sed dolor
                    stet amet
                  </p>
                  <div>
                    <a
                      onClick={handleClick}
                      href="#noPage"
                      className=" text-decoration-none me-2 read-more"
                    >
                      READ MORE
                      <i className="fa-solid fa-arrow-right ms-2"></i>
                    </a>
                  </div>
                </div>
              </div>
              <div className="col-md-6 col-sm-12 blog-grid-item mb-5">
                <div className="image position-relative">
                  <img src={Image2} alt="Image_Blog" className=" img-fluid" />
                  <div className="web-design text-white">
                    <a
                      onClick={handleClick}
                      href="#noPage"
                      className=" text-decoration-none text-white px-4 py-3"
                      style={{ backgroundColor: "var(--color-primary)" }}
                    >
                      Web Design
                    </a>
                  </div>
                </div>
                <div
                  style={{ backgroundColor: "var(--color-light-primary)" }}
                  className="p-3"
                >
                  <div className=" d-flex justify-content-between pt-2 mb-2">
                    <div>
                      <i
                        className="fa-solid fa-user me-2"
                        style={{ color: "var(--color-primary)" }}
                      ></i>{" "}
                      <span>Kim Hak</span>
                    </div>
                    <div>
                      <i
                        className="fa-solid fa-calendar-days me-2"
                        style={{ color: "var(--color-primary)" }}
                      ></i>{" "}
                      <span>01 Jan, 2024</span>
                    </div>
                  </div>
                  <h4 className="fw-bold ">How to build a website</h4>
                  <p>
                    Dolor et eos labore stet justo sed est sed sed sed dolor
                    stet amet
                  </p>
                  <div>
                    <a
                      onClick={handleClick}
                      href="#noPage"
                      className=" text-decoration-none me-2 read-more"
                    >
                      READ MORE
                      <i className="fa-solid fa-arrow-right ms-2"></i>
                    </a>
                  </div>
                </div>
              </div>
              <div className="col-md-6 col-sm-12 blog-grid-item mb-5">
                <div className="image position-relative">
                  <img src={Image3} alt="Image_Blog" className=" img-fluid" />
                  <div className="web-design text-white">
                    <a
                      onClick={handleClick}
                      href="#noPage"
                      className=" text-decoration-none text-white px-4 py-3"
                      style={{ backgroundColor: "var(--color-primary)" }}
                    >
                      Web Design
                    </a>
                  </div>
                </div>
                <div
                  style={{ backgroundColor: "var(--color-light-primary)" }}
                  className="p-3"
                >
                  <div className=" d-flex justify-content-between pt-2 mb-2">
                    <div>
                      <i
                        className="fa-solid fa-user me-2"
                        style={{ color: "var(--color-primary)" }}
                      ></i>{" "}
                      <span>Kim Hak</span>
                    </div>
                    <div>
                      <i
                        className="fa-solid fa-calendar-days me-2"
                        style={{ color: "var(--color-primary)" }}
                      ></i>{" "}
                      <span>01 Jan, 2024</span>
                    </div>
                  </div>
                  <h4 className="fw-bold ">How to build a website</h4>
                  <p>
                    Dolor et eos labore stet justo sed est sed sed sed dolor
                    stet amet
                  </p>
                  <div>
                    <a
                      onClick={handleClick}
                      href="#noPage"
                      className=" text-decoration-none me-2 read-more"
                    >
                      READ MORE
                      <i className="fa-solid fa-arrow-right ms-2"></i>
                    </a>
                  </div>
                </div>
              </div>
              <div className="col-md-6 col-sm-12 blog-grid-item mb-5">
                <div className="image position-relative">
                  <img src={Image1} alt="Image_Blog" className=" img-fluid" />
                  <div className="web-design text-white">
                    <a
                      onClick={handleClick}
                      href="#noPage"
                      className=" text-decoration-none text-white px-4 py-3"
                      style={{ backgroundColor: "var(--color-primary)" }}
                    >
                      Web Design
                    </a>
                  </div>
                </div>
                <div
                  style={{ backgroundColor: "var(--color-light-primary)" }}
                  className="p-3"
                >
                  <div className=" d-flex justify-content-between pt-2 mb-2">
                    <div>
                      <i
                        className="fa-solid fa-user me-2"
                        style={{ color: "var(--color-primary)" }}
                      ></i>{" "}
                      <span>Kim Hak</span>
                    </div>
                    <div>
                      <i
                        className="fa-solid fa-calendar-days me-2"
                        style={{ color: "var(--color-primary)" }}
                      ></i>{" "}
                      <span>01 Jan, 2024</span>
                    </div>
                  </div>
                  <h4 className="fw-bold ">How to build a website</h4>
                  <p>
                    Dolor et eos labore stet justo sed est sed sed sed dolor
                    stet amet
                  </p>
                  <div>
                    <a
                      onClick={handleClick}
                      href="#noPage"
                      className=" text-decoration-none me-2 read-more"
                    >
                      READ MORE
                      <i className="fa-solid fa-arrow-right ms-2"></i>
                    </a>
                  </div>
                </div>
              </div>
              <div className="col-md-6 col-sm-12 blog-grid-item mb-5">
                <div className="image position-relative">
                  <img src={Image2} alt="Image_Blog" className=" img-fluid" />
                  <div className="web-design text-white">
                    <a
                      onClick={handleClick}
                      href="#noPage"
                      className=" text-decoration-none text-white px-4 py-3"
                      style={{ backgroundColor: "var(--color-primary)" }}
                    >
                      Web Design
                    </a>
                  </div>
                </div>
                <div
                  style={{ backgroundColor: "var(--color-light-primary)" }}
                  className="p-3"
                >
                  <div className=" d-flex justify-content-between pt-2 mb-2">
                    <div>
                      <i
                        className="fa-solid fa-user me-2"
                        style={{ color: "var(--color-primary)" }}
                      ></i>{" "}
                      <span>Kim Hak</span>
                    </div>
                    <div>
                      <i
                        className="fa-solid fa-calendar-days me-2"
                        style={{ color: "var(--color-primary)" }}
                      ></i>{" "}
                      <span>01 Jan, 2024</span>
                    </div>
                  </div>
                  <h4 className="fw-bold ">How to build a website</h4>
                  <p>
                    Dolor et eos labore stet justo sed est sed sed sed dolor
                    stet amet
                  </p>
                  <div>
                    <a
                      onClick={handleClick}
                      href="#noPage"
                      className=" text-decoration-none me-2 read-more"
                    >
                      READ MORE
                      <i className="fa-solid fa-arrow-right ms-2"></i>
                    </a>
                  </div>
                </div>
              </div>
              <div className="col-md-6 col-sm-12 blog-grid-item mb-5">
                <div className="image position-relative">
                  <img src={Image3} alt="Image_Blog" className=" img-fluid" />
                  <div className="web-design text-white">
                    <a
                      onClick={handleClick}
                      href="#noPage"
                      className=" text-decoration-none text-white px-4 py-3"
                      style={{ backgroundColor: "var(--color-primary)" }}
                    >
                      Web Design
                    </a>
                  </div>
                </div>
                <div
                  style={{ backgroundColor: "var(--color-light-primary)" }}
                  className="p-3"
                >
                  <div className=" d-flex justify-content-between pt-2 mb-2">
                    <div>
                      <i
                        className="fa-solid fa-user me-2"
                        style={{ color: "var(--color-primary)" }}
                      ></i>{" "}
                      <span>Kim Hak</span>
                    </div>
                    <div>
                      <i
                        className="fa-solid fa-calendar-days me-2"
                        style={{ color: "var(--color-primary)" }}
                      ></i>{" "}
                      <span>01 Jan, 2024</span>
                    </div>
                  </div>
                  <h4 className="fw-bold ">How to build a website</h4>
                  <p>
                    Dolor et eos labore stet justo sed est sed sed sed dolor
                    stet amet
                  </p>
                  <div>
                    <a
                      onClick={handleClick}
                      href="#noPage"
                      className=" text-decoration-none me-2 read-more"
                    >
                      READ MORE
                      <i className="fa-solid fa-arrow-right ms-2"></i>
                    </a>
                  </div>
                </div>
              </div>
              <div className="col-md-6 col-sm-12 blog-grid-item mb-5">
                <div className="image position-relative">
                  <img src={Image1} alt="Image_Blog" className=" img-fluid" />
                  <div className="web-design text-white">
                    <a
                      onClick={handleClick}
                      href="#noPage"
                      className=" text-decoration-none text-white px-4 py-3"
                      style={{ backgroundColor: "var(--color-primary)" }}
                    >
                      Web Design
                    </a>
                  </div>
                </div>
                <div
                  style={{ backgroundColor: "var(--color-light-primary)" }}
                  className="p-3"
                >
                  <div className=" d-flex justify-content-between pt-2 mb-2">
                    <div>
                      <i
                        className="fa-solid fa-user me-2"
                        style={{ color: "var(--color-primary)" }}
                      ></i>{" "}
                      <span>Kim Hak</span>
                    </div>
                    <div>
                      <i
                        className="fa-solid fa-calendar-days me-2"
                        style={{ color: "var(--color-primary)" }}
                      ></i>{" "}
                      <span>01 Jan, 2024</span>
                    </div>
                  </div>
                  <h4 className="fw-bold ">How to build a website</h4>
                  <p>
                    Dolor et eos labore stet justo sed est sed sed sed dolor
                    stet amet
                  </p>
                  <div>
                    <a
                      onClick={handleClick}
                      href="#noPage"
                      className=" text-decoration-none me-2 read-more"
                    >
                      READ MORE
                      <i className="fa-solid fa-arrow-right ms-2"></i>
                    </a>
                  </div>
                </div>
              </div>
              <div className="col-md-6 col-sm-12 blog-grid-item mb-5">
                <div className="image position-relative">
                  <img src={Image2} alt="Image_Blog" className=" img-fluid" />
                  <div className="web-design text-white">
                    <a
                      onClick={handleClick}
                      href="#noPage"
                      className=" text-decoration-none text-white px-4 py-3"
                      style={{ backgroundColor: "var(--color-primary)" }}
                    >
                      Web Design
                    </a>
                  </div>
                </div>
                <div
                  style={{ backgroundColor: "var(--color-light-primary)" }}
                  className="p-3"
                >
                  <div className=" d-flex justify-content-between pt-2 mb-2">
                    <div>
                      <i
                        className="fa-solid fa-user me-2"
                        style={{ color: "var(--color-primary)" }}
                      ></i>{" "}
                      <span>Kim Hak</span>
                    </div>
                    <div>
                      <i
                        className="fa-solid fa-calendar-days me-2"
                        style={{ color: "var(--color-primary)" }}
                      ></i>{" "}
                      <span>01 Jan, 2024</span>
                    </div>
                  </div>
                  <h4 className="fw-bold ">How to build a website</h4>
                  <p>
                    Dolor et eos labore stet justo sed est sed sed sed dolor
                    stet amet
                  </p>
                  <div>
                    <a
                      onClick={handleClick}
                      href="#noPage"
                      className=" text-decoration-none me-2 read-more"
                    >
                      READ MORE
                      <i className="fa-solid fa-arrow-right ms-2"></i>
                    </a>
                  </div>
                </div>
              </div>
              <div className="col-md-6 col-sm-12 blog-grid-item mb-5">
                <div className="image position-relative">
                  <img src={Image3} alt="Image_Blog" className=" img-fluid" />
                  <div className="web-design text-white">
                    <a
                      onClick={handleClick}
                      href="#noPage"
                      className=" text-decoration-none text-white px-4 py-3"
                      style={{ backgroundColor: "var(--color-primary)" }}
                    >
                      Web Design
                    </a>
                  </div>
                </div>
                <div
                  style={{ backgroundColor: "var(--color-light-primary)" }}
                  className="p-3"
                >
                  <div className=" d-flex justify-content-between pt-2 mb-2">
                    <div>
                      <i
                        className="fa-solid fa-user me-2"
                        style={{ color: "var(--color-primary)" }}
                      ></i>{" "}
                      <span>Kim Hak</span>
                    </div>
                    <div>
                      <i
                        className="fa-solid fa-calendar-days me-2"
                        style={{ color: "var(--color-primary)" }}
                      ></i>{" "}
                      <span>01 Jan, 2024</span>
                    </div>
                  </div>
                  <h4 className="fw-bold ">How to build a website</h4>
                  <p>
                    Dolor et eos labore stet justo sed est sed sed sed dolor
                    stet amet
                  </p>
                  <div>
                    <a
                      onClick={handleClick}
                      href="#noPage"
                      className=" text-decoration-none me-2 read-more"
                    >
                      READ MORE
                      <i className="fa-solid fa-arrow-right ms-2"></i>
                    </a>
                  </div>
                </div>
              </div>
              <div className="col-md-6 col-sm-12 blog-grid-item mb-5">
                <div className="image position-relative">
                  <img src={Image1} alt="Image_Blog" className=" img-fluid" />
                  <div className="web-design text-white">
                    <a
                      onClick={handleClick}
                      href="#noPage"
                      className=" text-decoration-none text-white px-4 py-3"
                      style={{ backgroundColor: "var(--color-primary)" }}
                    >
                      Web Design
                    </a>
                  </div>
                </div>
                <div
                  style={{ backgroundColor: "var(--color-light-primary)" }}
                  className="p-3"
                >
                  <div className=" d-flex justify-content-between pt-2 mb-2">
                    <div>
                      <i
                        className="fa-solid fa-user me-2"
                        style={{ color: "var(--color-primary)" }}
                      ></i>{" "}
                      <span>Kim Hak</span>
                    </div>
                    <div>
                      <i
                        className="fa-solid fa-calendar-days me-2"
                        style={{ color: "var(--color-primary)" }}
                      ></i>{" "}
                      <span>01 Jan, 2024</span>
                    </div>
                  </div>
                  <h4 className="fw-bold ">How to build a website</h4>
                  <p>
                    Dolor et eos labore stet justo sed est sed sed sed dolor
                    stet amet
                  </p>
                  <div>
                    <a
                      onClick={handleClick}
                      href="#noPage"
                      className=" text-decoration-none me-2 read-more"
                    >
                      READ MORE
                      <i className="fa-solid fa-arrow-right ms-2"></i>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="col-lg-4 col-md-12">
            <div className="row">
              <div className="col-lg-9 col-md-9 col-sm-9 col-9 p-0">
                <input
                  type="text"
                  className="form-control rounded-end-0  rounded-start-0 p-3"
                  placeholder="Keyword"
                />
              </div>
              <div className="col-lg-3 col-md-3 col-sm-3 col-3 p-0">
                <button
                  className="form-control rounded-start-0 rounded-end-0 p-3"
                  style={{ color: "var(--color-primary)" }}
                >
                  <i className="fa-solid fa-search"></i>
                </button>
              </div>
              <div className="col-lg-3 col-md-12">
                <h3 className="mt-5 fw-bold">Categories</h3>
                <div className="loader  mb-3"></div>
              </div>
              <div className="col-12">
                <div className="web-designs">
                  <a
                    href="#noPage"
                    onClick={handleClick}
                    className=" text-decoration-none "
                  >
                    <i className="fa-solid fa-arrow-right me-3"></i>
                    Web Design
                  </a>
                </div>
              </div>
              <div className="col-12">
                <div className="web-designs">
                  <a
                    href="#noPage"
                    onClick={handleClick}
                    className=" text-decoration-none "
                  >
                    <i className="fa-solid fa-arrow-right me-3"></i>
                    Web Development
                  </a>
                </div>
              </div>
              <div className="col-12">
                <div className="web-designs">
                  <a
                    href="#noPage"
                    onClick={handleClick}
                    className=" text-decoration-none "
                  >
                    <i className="fa-solid fa-arrow-right me-3"></i>
                    Web Development
                  </a>
                </div>
              </div>
              <div className="col-12">
                <div className="web-designs">
                  <a
                    href="#noPage"
                    onClick={handleClick}
                    className=" text-decoration-none "
                  >
                    <i className="fa-solid fa-arrow-right me-3"></i>
                    Keyword Research
                  </a>
                </div>
              </div>
              <div className="col-12">
                <div className="web-designs">
                  <a
                    href="#noPage"
                    onClick={handleClick}
                    className=" text-decoration-none "
                  >
                    <i className="fa-solid fa-arrow-right me-3"></i>
                    Email Marketing
                  </a>
                </div>
              </div>
              <div className="col-12">
                <h3 className="mt-5 fw-bold">Recent Post</h3>
                <div className="loader  mb-3"></div>
              </div>
              <div className="col-12 d-flex mt-3">
                <div>
                  <img
                    src={Image1}
                    alt="Image_One"
                    style={{
                      width: "150px",
                      height: "130px",
                      objectFit: "cover",
                    }}
                  />
                </div>
                <div style={{ backgroundColor: "var(--color-light-primary)" }}>
                  <h5 className="p-2 mt-3 fw-bold">
                    Lorem, ipsum dolor sit amet adipis elit
                  </h5>
                </div>
              </div>
              <div className="col-12 d-flex mt-3">
                <div>
                  <img
                    src={Image2}
                    alt="Image_One"
                    style={{
                      width: "150px",
                      height: "130px",
                      objectFit: "cover",
                    }}
                  />
                </div>
                <div style={{ backgroundColor: "var(--color-light-primary)" }}>
                  <h5 className="p-2 mt-3 fw-bold">
                    Lorem, ipsum dolor sit amet adipis elit
                  </h5>
                </div>
              </div>
              <div className="col-12 d-flex mt-3">
                <div>
                  <img
                    src={Image3}
                    alt="Image_One"
                    style={{
                      width: "150px",
                      height: "130px",
                      objectFit: "cover",
                    }}
                  />
                </div>
                <div style={{ backgroundColor: "var(--color-light-primary)" }}>
                  <h5 className="p-2 mt-3 fw-bold">
                    Lorem, ipsum dolor sit amet adipis elit
                  </h5>
                </div>
              </div>
              <div className="col-12 d-flex mt-3">
                <div>
                  <img
                    src={Image1}
                    alt="Image_One"
                    style={{
                      width: "150px",
                      height: "130px",
                      objectFit: "cover",
                    }}
                  />
                </div>
                <div style={{ backgroundColor: "var(--color-light-primary)" }}>
                  <h5 className="p-2 mt-3 fw-bold">
                    Lorem, ipsum dolor sit amet adipis elit
                  </h5>
                </div>
              </div>
              <div className="col-12 d-flex mt-3">
                <div>
                  <img
                    src={Image2}
                    alt="Image_One"
                    style={{
                      width: "150px",
                      height: "130px",
                      objectFit: "cover",
                    }}
                  />
                </div>
                <div style={{ backgroundColor: "var(--color-light-primary)" }}>
                  <h5 className="p-2 mt-3 fw-bold">
                    Lorem, ipsum dolor sit amet adipis elit
                  </h5>
                </div>
              </div>
              <div className="col-12 d-flex mt-3">
                <div>
                  <img
                    src={Image3}
                    alt="Image_One"
                    style={{
                      width: "150px",
                      height: "130px",
                      objectFit: "cover",
                    }}
                  />
                </div>
                <div style={{ backgroundColor: "var(--color-light-primary)" }}>
                  <h5 className="p-2 mt-3 fw-bold">
                    Lorem, ipsum dolor sit amet adipis elit
                  </h5>
                </div>
              </div>
              <div className="col-12 mt-4">
                <img
                  src={Image1}
                  alt="Image_one"
                  className=" img-fluid w-100"
                />
              </div>
              <div className="col-12">
                <h3 className="mt-5 fw-bold">Tag Cloud</h3>
                <div className="loader  mb-3"></div>
              </div>
              <div className="col-12 d-flex flex-wrap">
                <div
                  style={{ backgroundColor: "var(--color-light-primary)" }}
                  className="px-4 py-2 me-3 mt-2"
                >
                  Design
                </div>
                <div
                  style={{ backgroundColor: "var(--color-light-primary)" }}
                  className="px-4 py-2 me-3 mt-2"
                >
                  Development
                </div>
                <div
                  style={{ backgroundColor: "var(--color-light-primary)" }}
                  className="px-4 py-2 me-3 mt-2"
                >
                  Marketing
                </div>
                <div
                  style={{ backgroundColor: "var(--color-light-primary)" }}
                  className="px-4 py-2 me-3 mt-2"
                >
                  SEO
                </div>
                <div
                  style={{ backgroundColor: "var(--color-light-primary)" }}
                  className="px-4 py-2 me-3 mt-2"
                >
                  Writing
                </div>
                <div
                  style={{ backgroundColor: "var(--color-light-primary)" }}
                  className="px-4 py-2 me-3 mt-2"
                >
                  Consulting
                </div>
                <div
                  style={{ backgroundColor: "var(--color-light-primary)" }}
                  className="px-4 py-2 me-3 mt-2"
                >
                  Design
                </div>
                <div
                  style={{ backgroundColor: "var(--color-light-primary)" }}
                  className="px-4 py-2 me-3 mt-2"
                >
                  Development
                </div>
                <div
                  style={{ backgroundColor: "var(--color-light-primary)" }}
                  className="px-4 py-2 me-3 mt-2"
                >
                  Marketing
                </div>
                <div
                  style={{ backgroundColor: "var(--color-light-primary)" }}
                  className="px-4 py-2 me-3 mt-2"
                >
                  SEO
                </div>
                <div
                  style={{ backgroundColor: "var(--color-light-primary)" }}
                  className="px-4 py-2 me-3 mt-2"
                >
                  Writng
                </div>
                <div
                  style={{ backgroundColor: "var(--color-light-primary)" }}
                  className="px-4 py-2 me-3 mt-2"
                >
                  Consulting
                </div>
              </div>
              <div className="col-12 ">
                <h3 className="mt-5 fw-bold">Plain Text</h3>
                <div className="loader  mb-3"></div>
                <div
                  style={{ backgroundColor: "var(--color-light-primary)" }}
                  className="p-3 text-center"
                >
                  <p>
                    Vero sea et accusam justo dolor accusam lorem consetetur,
                    dolores sit amet sit dolor clita kasd justo, diam accusam no
                    sea ut tempor magna takimata, amet sit et diam dolor ipsum
                    amet diam
                  </p>
                  <button
                    className="border-0 px-4 py-2 text-white mt-1 mb-3"
                    style={{ backgroundColor: "var(--color-primary)" }}
                  >
                    Read More
                  </button>
                </div>
              </div>
            </div>
          </div>
          <nav aria-label="Page navigation example">
            <ul className="pagination mt-5">
              <li className="page-item">
                <a
                  onClick={handleClick}
                  className="page-link px-md-4 py-md-3 px-sm-2 py-sm-2"
                  style={{ color: "var(--color-primary)" }}
                  href="#noPage"
                  aria-label="Previous"
                >
                  <span aria-hidden="true">
                    <i className="fa-solid fa-arrow-left"></i>
                  </span>
                  <span className="sr-only">Previous</span>
                </a>
              </li>
              <li className="page-item">
                <a
                  onClick={handleClick}
                  className="page-link  px-md-4 py-md-3 px-sm-2 py-sm-2"
                  style={{ color: "var(--color-primary)" }}
                  href="#noPage"
                >
                  1
                </a>
              </li>
              <li className="page-item">
                <a
                  onClick={handleClick}
                  className="page-link  px-md-4 py-md-3 px-sm-2 py-sm-2"
                  style={{ color: "var(--color-primary)" }}
                  href="#noPage"
                >
                  2
                </a>
              </li>
              <li className="page-item">
                <a
                  onClick={handleClick}
                  className="page-link  px-md-4 py-md-3 px-sm-2 py-sm-2"
                  style={{ color: "var(--color-primary)" }}
                  href="#noPage"
                >
                  3
                </a>
              </li>
              <li className="page-item">
                <a
                  onClick={handleClick}
                  className="page-link  px-md-4 py-md-3 px-sm-2 py-sm-2"
                  href="#noPage"
                  aria-label="Next"
                  style={{ color: "var(--color-primary)" }}
                >
                  <span aria-hidden="true">
                    <i className="fa-solid fa-arrow-right"></i>
                  </span>
                  <span className="sr-only">Next</span>
                </a>
              </li>
            </ul>
          </nav>
        </div>
      </div>
      <VendorCarousel />
      <Footer />
      <BackToTopCom />
    </div>
  );
};

export default BlogGridPage;
