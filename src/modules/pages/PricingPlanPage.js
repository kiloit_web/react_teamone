import React from "react";
import PricingPlanCom from "../../components/PricingPlanCom";
import VendorCarousel from "../../components/VendorCarousel";
import Footer from "../layouts/footer/Footer";
import BackToTopCom from "../../components/BackToTopCom";

const PricingPlanPage = () => {
  return (
    <div>
      <PricingPlanCom />
      <VendorCarousel/>
      <Footer/>
      <BackToTopCom/>
    </div>
  );
};

export default PricingPlanPage;
