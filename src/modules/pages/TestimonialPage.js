import React from "react";
import Testmonial from "../../components/TestmonialCom";
import VendorCarousel from "../../components/VendorCarousel";
import Footer from "../layouts/footer/Footer";
import BackToTopCom from "../../components/BackToTopCom";

const TestimonialPage = () => {
  return (
    <div>
      <Testmonial/>
      <VendorCarousel/>
      <Footer/>
      <BackToTopCom/>
    </div>
  );
};

export default TestimonialPage;
