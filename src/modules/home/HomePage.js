import AboutUseCom from "../../components/AboutUseCom";
import BackToTopCom from "../../components/BackToTopCom";
import ChooseUsCom from "../../components/ChooseUsCom";
import PricingPlanCom from "../../components/PricingPlanCom";
import ServiceCom from "../../components/ServiceCom";
import Testmonial from "../../components/TestmonialCom";
import VendorCarousel from "../../components/VendorCarousel";
import Footer from "../layouts/footer/Footer";

const HomePage = () => {
  return (
    <div>
      
      <AboutUseCom />
      <ChooseUsCom />
      <ServiceCom />
      <PricingPlanCom />
      <Testmonial />
      <VendorCarousel/>
      <BackToTopCom/>
      <Footer/>
    </div>
  );
};

export default HomePage;
