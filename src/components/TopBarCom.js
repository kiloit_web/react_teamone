import React from "react";
import "../assets/css/main.css";

const TopBarCom = () => {
  return (
    <div className=" d-xl-block d-lg-none d-none ">
      <div className="top-bar text-white px-5">
        <div className=" container-fluid p-2 fs-6 d-flex justify-content-between">
          <div>
            <small>
              <i className="text-white fa-solid me-2 fa-location-dot"></i>{" "}
              <span>123 Street, New York, USA</span>
            </small>
            <small className="ms-4">
              <i className="text-white fa-solid me-2 fa-phone"></i> <span>+012 345 6789</span>
            </small>
            <small className="ms-4">
              <i className="text-white fa-solid me-2 fa-envelope"></i>{" "}
              <span>info@example.com</span>
            </small>
          </div>
          <div className="top-icon fs-6 text-center d-flex">
            <div className=" ms-3">
              <a href="https://twitter.com/" target="_blank" rel="noreferrer">
                <i className=" fa-brands rounded-circle-border fa-twitter"></i>
              </a>
            </div>
            <div className=" ms-3">
              <a
                href="https://www.facebook.com/profile.php?id=100014518718675"
                target="_blank"
                rel="noreferrer"
              >
                <i className=" fa-brands rounded-circle-border fa-facebook"></i>
              </a>
            </div>
            <div className=" ms-3">
              <a
                href="https://www.linkedin.com/feed/?trk=404_page"
                target="_blank"
                rel="noreferrer"
              >
                <i className=" fa-brands rounded-circle-border fa-linkedin"></i>
              </a>
            </div>
            <div className=" ms-3">
              <a
                href="https://www.instagram.com/heang__kimhak/"
                target="_blank"
                rel="noreferrer"
              >
                <i className=" fa-brands rounded-circle-border fa-instagram"></i>
              </a>
            </div>
            <div className=" ms-3">
              <a
                href="https://www.youtube.com/channel/UCJbtn8BDMcJ7a5wyMiQ_4FA"
                target="_blank"
                rel="noreferrer"
              >
                <i className=" fa-brands fa-youtube rounded-circle-border"></i>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default TopBarCom;
