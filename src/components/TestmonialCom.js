import React, { useEffect } from "react";
import "../assets/css/main.css";
import AOS from "aos";
import "aos/dist/aos.css";
import OwlCarousel from "react-owl-carousel";
import "owl.carousel/dist/assets/owl.carousel.css";
import "owl.carousel/dist/assets/owl.theme.default.css";
import image1 from "../assets/image/testmonial1.jpg";
import image2 from "../assets/image/testmonial2.jpg";
import image3 from "../assets/image/testmonial3.jpg";
import image4 from "../assets/image/testmonial4.jpg";

const Testmonial = () => {
  useEffect(() => {
    AOS.init();
  }, []);
  const options = {
    loop: true,
    margin: 20,
    responsiveClass: true,
    autoplay: true,
    dotsEach: true,
    animateOut: true,
    animateIn: true,
    slidetransition: "linear",
    autoplayTimeout: 5000,
    autoplaySpeed: 1500,
    autoplayHoverPause: true,
    responsive: {
      0: {
        items: 1,
      },
      300: {
        items: 1,
      },
      600: {
        items: 2,
      },
      1000: {
        items: 2,
      },
      1200: {
        items: 3,
      },
    },
  };
  return (
    <div className="container margin-3rem">
      <div data-aos="fade-up" data-aos-duration="1000" data-aos-easing="linear">
        <h5
          className=" fw-bold text-center"
          style={{ color: "var(--color-primary)" }}
        >
          TESTIMONIAL
        </h5>
        <div className="text-center">
          <h1 className="about-text  fw-bold w-50 mx-auto">
            What Our Clients Say About Our Digital Services
          </h1>
        </div>
        <div className="loader mb-5 mx-auto"> </div>
        {/* // className "owl-theme" is optional ***************************************/}
        <OwlCarousel className="owl-theme" {...options}>
          <div
            className="item p-5"
            style={{ backgroundColor: "var(--color-light-primary)" }}
          >
            <div className=" d-flex justify-content-around">
              <div className="icon showdow">
                <img src={image1} alt="Testmonial1" />
              </div>
              <div>
                <h4
                  style={{ color: "var(--color-primary)" }}
                  className="fw-bold"
                >
                  Client Name
                </h4>
                <p>
                  <small>PROFESSIONAL</small>
                </p>
              </div>
            </div>
            <hr />
            <div>
              Dolor et eos labore, stet justo sed est sed. Diam sed sed dolor
              stet amet eirmod eos labore diam.
            </div>
          </div>
          <div
            className="item p-5"
            style={{ backgroundColor: "var(--color-light-primary)" }}
          >
            <div className=" d-flex justify-content-around">
              <div className="icon">
                <img src={image2} alt="Testmonial1" />
              </div>
              <div>
                <h4
                  style={{ color: "var(--color-primary)" }}
                  className="fw-bold"
                >
                  Client Name
                </h4>
                <p>
                  <small>PROFESSIONAL</small>
                </p>
              </div>
            </div>
            <hr />
            <div>
              Dolor et eos labore, stet justo sed est sed. Diam sed sed dolor
              stet amet eirmod eos labore diam.
            </div>
          </div>
          <div
            className="item p-5"
            style={{ backgroundColor: "var(--color-light-primary)" }}
          >
            <div className=" d-flex justify-content-around">
              <div className="icon">
                <img src={image3} alt="Testmonial1" />
              </div>
              <div>
                <h4
                  style={{ color: "var(--color-primary)" }}
                  className="fw-bold"
                >
                  Client Name
                </h4>
                <p>
                  <small>PROFESSIONAL</small>
                </p>
              </div>
            </div>
            <hr />
            <div>
              Dolor et eos labore, stet justo sed est sed. Diam sed sed dolor
              stet amet eirmod eos labore diam.
            </div>
          </div>
          <div
            className="item p-5"
            style={{ backgroundColor: "var(--color-light-primary)" }}
          >
            <div className=" d-flex justify-content-around">
              <div className="icon">
                <img src={image4} alt="Testmonial1" />
              </div>
              <div>
                <h4
                  style={{ color: "var(--color-primary)" }}
                  className="fw-bold"
                >
                  Client Name
                </h4>
                <p>
                  <small>PROFESSIONAL</small>
                </p>
              </div>
            </div>
            <hr />
            <div>
              Dolor et eos labore, stet justo sed est sed. Diam sed sed dolor
              stet amet eirmod eos labore diam.
            </div>
          </div>
        </OwlCarousel>
      </div>
    </div>
  );
};

export default Testmonial;
