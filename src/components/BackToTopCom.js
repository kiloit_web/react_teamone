import React from "react";

const BackToTopCom = () => {
  const handleClick = () => {
    window.scrollTo(0, 0);
  };
  return (
    <div>
      <div className="icons">
        <a href="#noReference" onClick={handleClick}>
          <i className="fa-solid fa-arrow-up"></i>
        </a>
      </div>
    </div>
  );
};

export default BackToTopCom;
