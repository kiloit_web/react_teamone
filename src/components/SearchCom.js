import { useState } from "react";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";

function SearchCom() {
  const [show, setShow] = useState(false);
  const fullscreen = true; // Set fullscreen to true by default

  function handleShow() {
    setShow(true);
  }
  return (
    <>
      <Button
        className="bg-white border-0 p-0"
        style={{ color: "var(--color-primary)" }}
        onClick={handleShow}
      >
        <i className="fa-solid fa-magnifying-glass"></i>
      </Button>
      <Modal
        show={show}
        fullscreen={fullscreen}
        onHide={() => setShow(false)}
        className="searchMode"
      >
        <Modal.Header closeButton className="border-bottom-0"></Modal.Header>
        <Modal.Body className="d-flex justify-content-center align-items-center">
          <div>
            <input
              type="text"
              className=" px-3 py-2 form-control rounded-top-0 rounded-bottom-0"
              style={{ borderColor: "var(--color-primary)" }}
            />
          </div>
          <div>
            <button
              className=" px-5 py-2 form-control rounded-top-0 rounded-bottom-0"
              style={{
                borderColor: "var(--color-primary)",
                backgroundColor: "var(--color-primary)",
              }}
            >
              <i className="fa-solid fa-magnifying-glass text-white"></i>
            </button>
          </div>
        </Modal.Body>
      </Modal>
    </>
  );
}

export default SearchCom;
