import React, { useEffect } from "react";
import "../assets/css/main.css";
import AboutPic from "../assets/image/about-pic.jpg";
import AOS from "aos";
import "aos/dist/aos.css";
import { useNavigate } from "react-router-dom";

const AboutUseCom = () => {
  useEffect(() => {
    AOS.init();
  }, []);
  const navigate = useNavigate();
  const freeQuote = () => {
    navigate("/freeQuote");
  };
  return (
    <div className="container margin-3rem">
      <div
        className="row"
        data-aos="fade-up"
        data-aos-duration="1000"
        data-aos-easing="linear"
      >
        <div className="col-lg-7 col-md-12">
          <h5 className=" fw-bold" style={{ color: "var(--color-primary)" }}>
            ABOUT US
          </h5>
          <h1 className="about-text  fw-bold">
            The Best IT Solution With 10 Years of Experience
          </h1>

          <div className="loader mb-0"> </div>
          <p
            className="mt-4 mb-4 "
            style={{ color: "var(--color-lightwhite)" }}
          >
            Tempor erat elitr rebum at clita. Diam dolor diam ipsum et tempor
            sit. Aliqu diam amet diam et eos labore. Clita erat ipsum et lorem
            et sit, sed stet no labore lorem sit. Sanctus clita duo justo et
            tempor eirmod magna dolore erat amet
          </p>
          <div className=" d-lg-flex d-md-block fw-bold ">
            <div data-aos="zoom-in" data-aos-duration="1000">
              <div>
                <i
                  className="fa-solid me-3 fa-check"
                  style={{ color: "var(--color-primary)" }}
                ></i>
                Award Winning
              </div>
              <div>
                <i
                  className="fa-solid me-3 fa-check"
                  style={{ color: "var(--color-primary)" }}
                ></i>
                Professional Staff
              </div>
            </div>
            <div
              className=" ms-lg-5 ms-md-0"
              data-aos="zoom-in"
              data-aos-duration="1000"
            >
              <div>
                <i
                  className="fa-solid me-3 fa-check"
                  style={{ color: "var(--color-primary)" }}
                ></i>
                24/7 Support
              </div>
              <div>
                <i
                  className="fa-solid me-3 fa-check"
                  style={{ color: "var(--color-primary)" }}
                ></i>
                Fair Prices
              </div>
            </div>
          </div>
          <div className=" d-flex  mt-4">
            <div
              className="icon fs-5 phone text-center me-4 mt-2"
              style={{ backgroundColor: "var(--color-primary)" }}
            >
              <i className="fa-solid fa-phone"></i>
            </div>
            <div>
              <div>Call to ask any question</div>
              <div
                className="fs-5 fw-bold"
                style={{ color: "var(--color-primary)" }}
              >
                +012 345 6789
              </div>
            </div>
          </div>
          <button
            className=" border-0 py-2 px-4 text-white mt-4 mb-4"
            style={{ backgroundColor: "var(--color-primary)" }}
            data-aos="zoom-in"
            data-aos-duration="1000"
            onClick={freeQuote}
          >
            Request a quote
          </button>
        </div>
        <div className="col-lg-5 col-md-12">
          <div className="about-us d-xl-flex d-lg-block">
            <div></div>
            <div
              className="about-pic w-100"
              data-aos="zoom-in"
              data-aos-duration="1000"
            >
              <img src={AboutPic} alt="About_Picture" className=" img-fluid" />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AboutUseCom;
